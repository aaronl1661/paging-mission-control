import json
import sys
from telemetryRead import scanInputRegex
from triggerAlarm import jsonOutput


def main(args):

    if len(args) == 0:
        print("In order to run the file, type python3 driver.py <filename.txt>")
        return

    with open(args):
        # Scans the input file for the telemetry reads
        inputArr = scanInputRegex(args)

        # Outputs necessary alerts
        output = json.dumps(jsonOutput(inputArr), indent=4)
        print(output)
    return output


if __name__ == "__main__":
    args = sys.argv[1:]
    main(args[0])
