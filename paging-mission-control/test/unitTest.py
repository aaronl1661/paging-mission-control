import unittest
import sys

sys.path.append('..')

from main import main


class TestStringMethods(unittest.TestCase):
    # Testing Base Case
    def testBaseCaseValidation(self):
        with open('validateOutput.txt') as f:
            lines = f.read()
        self.assertEqual(main("validateInput.txt"), lines)

    # Testing Time Interval
    def testTimeIntervalCaseValidation(self):
        with open('tiOutput.txt') as f:
            lines = f.read()
        self.assertEqual(main("tiInput.txt"), lines)


if __name__ == '__main__':
    unittest.main()
