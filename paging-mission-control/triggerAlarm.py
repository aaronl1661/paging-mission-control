import re
import datetime

# Minute Interval
MINS = 5

# Number of violations before sending error
MAX_VIOLATIONS = 3

# Severity Types
TSTAT_SEVERITY = "RED HIGH"
BATT_SEVERITY = "RED LOW"


# Helper function that returns true if time difference is greater than interval, else false
def timeDifference(startTime, endTime):

    if (endTime - startTime).total_seconds() > MINS*60:
        return True
    return False


# Appends to JSON array if an alert is necessary
# Helper function for jsonOutput
def alertType(alertDict, timeAlertArr, satID, raw, alert, time, comp, jsonArr, severity, i, comparison):

    # If the alert type dict does not contain a value containing the id, create it
    # Dict values are {Satellite ID : {Violation Count, Time Index}}
    if alertDict.get(satID) is None:
        alertDict[satID] = {"violationCount": 0,
                            "timeIndex": i}
    # Comparison specific to type of component
    # gt = TSTAT
    # lt = BATT
    if comparison == "gt":
        # If true then limit is violated
        if float(raw) > float(alert):
            timeAlertArr.append({"datetime": time,
                                "violationBoolean": True})
            alertDict[satID]["violationCount"] += 1
        else:
            timeAlertArr.append({"datetime": time,
                                "violationBoolean": False})
    elif comparison == "lt":
        # If true then limit is violated
        if float(raw) < float(alert):
            timeAlertArr.append({"datetime": time,
                                "violationBoolean": True})
            alertDict[satID]["violationCount"] += 1
        else:
            timeAlertArr.append({"datetime": time,
                                "violationBoolean": False})

    # While the window of time is still above the given interval, keep shifting the time array
    while timeDifference(timeAlertArr[alertDict[satID]["timeIndex"]]["datetime"],
                         timeAlertArr[i]["datetime"]):
        # If the left end of the window is a violation, remove it and shift the left index by one
        if timeAlertArr[alertDict[satID]["timeIndex"]]["violationBoolean"]:
            alertDict[satID]["violationCount"] -= 1
        alertDict[satID]["timeIndex"] += 1

    # Checks for max number of violations within the given time interval
    if alertDict.get(satID)["violationCount"] >= MAX_VIOLATIONS:
        # Z is hardcoded since there is no info on timezone given
        jsonArr.append({"satelliteId": satID, "severity": severity,
                        "component": comp,
                        "timestamp": timeAlertArr[alertDict[satID]["timeIndex"]]
                                        ["datetime"].strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"})


# Takes regex-ed variables as a tuple in array and outputs as formatted json
def jsonOutput(regexArr):

    # Universal array with starting and current indices for sliding windows for battery and temperature and satellite ID
    # Array values are formatted as index:{Datetime, Violation Boolean}
    timeAlertArr = []

    # Dict values are {Satellite ID : {Violation Count, Time Index}}
    tempAlert = {}
    battAlert = {}

    # Output array with violation alerts
    jsonArr = []

    # Iterates through telemetries
    for i, expr in enumerate(regexArr):
        # Extracts information from regex values
        timestamp, satID, redHL, yellowHL, yellowLL, redLL, raw, comp = expr
        # Casts String satID to Integer
        satID = int(satID)

        # Converts timestamp to type datetime
        timeStr = re.search(r"(\d{4})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d*)", timestamp)
        time = datetime.datetime(int(timeStr.group(1)), int(timeStr.group(2)),
                                 int(timeStr.group(3)), int(timeStr.group(4)),
                                 int(timeStr.group(5)), int(timeStr.group(6)),
                                 int(timeStr.group(7))*1000)

        # Checks if component type is a Thermostat or Battery
        if comp == "TSTAT":
            # Appends an alert to output array if necessary for temps
            alertType(tempAlert, timeAlertArr, satID, raw, redHL, time, comp, jsonArr, TSTAT_SEVERITY, i, "gt")

        elif comp == "BATT":
            # Appends an alert to output array if necessary for batteries
            alertType(battAlert, timeAlertArr, satID, raw, redLL, time, comp, jsonArr, BATT_SEVERITY, i, "lt")

    return jsonArr
