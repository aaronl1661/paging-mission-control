import re

# Regex values for each type of value inputted
# Type is unknown so we match all characters

RE_TIMESTAMP = ".*"
RE_SATELLITE_ID = ".*"
RE_RED_HL = ".*"
RE_YELLOW_HL = ".*"
RE_YELLOW_LL = ".*"
RE_RED_LL = ".*"
RE_RAW_VALUE = ".*"
RE_COMPONENT = ".*"


# Takes string input and returns a tuple as
# (Timestamp, Satellite, Red High Limit, Yellow High Limit, Yellow Low Limit, Red Low Limit, Raw Value, Component)
def regExp(input):

    pattern = re.compile(rf"({RE_TIMESTAMP})\|"
                         rf"({RE_SATELLITE_ID})\|"
                         rf"({RE_RED_HL})\|"
                         rf"({RE_YELLOW_HL})\|"
                         rf"({RE_YELLOW_LL})\|"
                         rf"({RE_RED_LL})\|"
                         rf"({RE_RAW_VALUE})\|"
                         rf"({RE_COMPONENT})")

    regex = re.search(pattern, input)
    return (regex.group(1), regex.group(2), regex.group(3), regex.group(4),
            regex.group(5), regex.group(6), regex.group(7), regex.group(8))


# Scans the input file line by line and regexs it and outputs as an array
def scanInputRegex(fileInput):
    strInput = []

    with open(fileInput, "r") as f:
        for line in f:
            regex = regExp(line)
            strInput.append(regex)

    return strInput
