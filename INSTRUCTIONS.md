# Paging Mission Control

This package takes an input file of telemetry reads and outputs alerts in JSON in console.
With a given alert threshold and time interval, the program will alert based on given parameters.

## Required libraries

This package involves all default packages installed with Python, so no other libraries need to be installed.
This program utilizes Python 3.8.9

## Input Format
The program accepts a file as input. The file is an ASCII text file containing pipe delimited records.

The ingest of status telemetry data has the format:

```
<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
```

## Output Format
The output will specify alert messages.  The alert messages are valid JSON with the following properties:

```javascript
{
    "satelliteId": 1234,
    "severity": "severity",
    "component": "component",
    "timestamp": "timestamp"
}
```

## Assumptions

This program assumes: 
- Valid input files
- Time entries are in order
- Satellite Id is an integer

## Unit Testing

This program tests the base test case given in the README.md
To run it, type python3 unitTest.py

To run the program through main, use python3 main.py <filename.txt>